<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Http\Requests\CompanyCreateRequest;
use App\Http\Requests\CompanyUpdateRequest;
use Illuminate\Http\Request;
use Storage;

class CompaniesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::where(function ($query) {
            $query->where('name', 'like', '%'.request('q').'%');
        })->paginate(10);

        return view('companies.index')->with('company', $companies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $company = Company::create($request->all());

        return redirect()->route('home', $company);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        $editCompany = null;
        $company = Company::find($company);

        if (in_array(request('action'), ['edit']) && request('id') != null) {
            $editCompany = Company::find(request('id'));
        }
        
        return view('companies.show', compact('company', 'editCompany'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return view ('companies.edit', compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        
        $request->validate([
            'name'  => 'required',
            'email' => 'required',
            'website' => 'required',
        ]);

        // Company::where('id', $company->id)
        //     ->update([
        //         'name'  => $request->name,
        //         'email' => $request->email,
        //         'website' => $request->website,
        //     ]);

        $company->update($request->all());
        if($request->hasFile('logo')) 
        {
            request->file('avatar')->move('images/',$request->file('logo')->getClientOriginalName());
            $company->logo = $request->file('logo')->getClientOriginalName();
            $siswa->save();
        }

        return redirect()->route('company.show', compact('company'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        Company::destroy($company->id);
        return redirect()->route('home');
    }
}
