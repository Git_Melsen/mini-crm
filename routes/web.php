<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () { 

    Route::get('/home', [App\Http\Controllers\CompaniesController::class, 'index'])->name('home');

    // Route::resources([
    //     'company' => CompaniesController::class,
    // ]);

    Route::post('/home', [App\Http\Controllers\CompaniesController::class, 'store'])->name('company.store');
    Route::get('/home/{company}', [App\Http\Controllers\CompaniesController::class, 'show'])->name('company.show');
    Route::get('/home/{company}/edit', [App\Http\Controllers\CompaniesController::class, 'edit'])->name('company.edit');
    Route::patch('/home/{company}/update', [App\Http\Controllers\CompaniesController::class, 'update'])->name('company.update');
    Route::delete('/home/{company}', [App\Http\Controllers\CompaniesController::class, 'destroy'])->name('company.destroy');



    Route::get('/employee', [App\Http\Controllers\EmployeesController::class, 'index'])->name('employee.index');
    Route::post('/employee/submit', [App\Http\Controllers\EmployeesController::class, 'store'])->name('employee.store');
    Route::get('/employee/{employee}', [App\Http\Controllers\EmployeesController::class, 'show'])->name('employee.show');
    Route::get('/employee/{employee}/edit', [App\Http\Controllers\EmployeesController::class, 'edit'])->name('employee.edit');
    Route::patch('/employee/{employee}/update', [App\Http\Controllers\EmployeesController::class, 'update'])->name('employee.update');
    Route::delete('/employee/{employee}', [App\Http\Controllers\EmployeesController::class, 'destroy'])->name('employee.destroy');

});
