@extends('layouts.master')

@section('main-menu')
        <div class="main">
            <!-- MAIN CONTENT -->
            <div class="main-content">
                <div class="container-fluid">
                    <h3 class="page-title"></h3>
                    <div id="toastr-demo" class="panel">
                        <div class="panel-body">
                            @if(session('status'))
                                <div class="alert alert-success text-center" role="alert">
                                    {{session('status')}}
                                </div>
                            @endif
                            <!-- CONTEXTUAL -->
                            <!-- <form> -->
                                <h2 class="text-center">List Company</h2>

                                
                                    <!-- CONDENSED TABLE -->
                                    <div class="panel">
                                        <div class="col-6">
                                            <!-- Button trigger modal -->
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                              Add
                                            </button>

                                        </div>
                                        <div class="panel-body">
                                            <table class="table table-condensed table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Company Name</th>
                                                        <th>Email</th>
                                                        <th>Website</th>
                                                        <th>phone</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @foreach($listEmployee as $key => $employee)
                                                    <tr>
                                                        <td>{{ $loop->iteration }}</td>
                                                        <td>{{ $employee->first_name }}</td>
                                                        <td>{{ $employee->email }}</td>
                                                        <td>{{ $employee->website }}</td>
                                                        <td>{{ $employee->phone }}</td>
                                                        <td>
                                                            <a href="{{ route ('employee.show', ['employee' => $employee->id]) }}" class="badge badge-info">detail</a>
                                                        </td>
                                                    </tr>
                                                    @endforeach

                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- END CONDENSED TABLE -->
                                



                            <!-- </form> -->
                            <!-- END CALLBACK -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT -->
        </div>
@stop






<!-- Modal Employee Insert -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Employee Insert</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('employee.store') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="first_name">First Name</label>
                            <input type="text" class="form-control @error ('first_name') is-invalid @enderror" id="first_name" name="first_name" aria-describedby="emailHelp" value="{{ old('first_name') }}">

                            @error ('first_name')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="las_name">Last Name</label>
                            <input type="text" class="form-control @error ('last_name') is-invalid @enderror" id="last_name" name="last_name" aria-describedby="emailHelp" value="{{ old('last_name') }}">

                            @error ('last_name')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="exampleFormControlSelect2">Company Name</label>
                            @foreach($listCompany as $company)
                            <select class="form-control" id="company_id" name="company_id">
                                <option>{{ $company->id }}</option>
                            </select>
                            @endforeach         
                        </div>

                        <div class="form-group">
                            <label for="email">Email address</label>
                            <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp">
                        </div>

                        <div class="form-group">
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control @error ('phone') is-invalid @enderror" id="phone" name="phone" aria-describedby="emailHelp" value="{{ old('phone') }}">

                            @error ('phone')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
            </div>
        </div>
    </div>
</div>



