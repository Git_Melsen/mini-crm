<div id="sidebar-nav" class="sidebar">
	<div class="sidebar-scroll">
		<nav>
			<ul class="nav">
				
				<li><a href="{{ route('home') }}" class="{{ Request::is('home') ? 'active' : '' }}"><i class="lnr lnr-home"></i> <span>Company</span></a></li>
				<li><a href="{{ route('employee.index') }}" class="{{ Request::is('employee') ? 'active' : '' }}"><i class="lnr lnr-user"></i> <span>Employee</span></a></li>
				
			</ul>
		</nav>
	</div>
</div>